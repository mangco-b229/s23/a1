

const trainer = {
    Name: "Ash Ketchum",
    Age: 10,
    Pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
    Friends: {
        hoenn: ["Misty", "Brock"],
        kanto: ["Gary Oak", "Tracey Sketchit"],
    },
    talk: function() {
        console.log("Pikachu! I choose you!");
    }
};

console.log(trainer)

console.log("Result of dot notation:")
console.log(trainer.Name)

console.log("Result of bracket notation:")
console.log(trainer["Pokemon"])

console.log("Result of talk method:")
trainer.talk()